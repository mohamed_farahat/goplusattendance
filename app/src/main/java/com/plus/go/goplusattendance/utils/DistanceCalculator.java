package com.plus.go.goplusattendance.utils;

import android.location.Location;
import android.util.Log;

/**
 * Created by raed on 12/22/18.
 */

public class DistanceCalculator {

    private static final String TAG = "DistanceCalculator";

    //    public static boolean locationIdintity(double unitLat, double unitLon, double detectedLatitude, double detectedLongitude) {
//
//        return distance(unitLat, unitLon, detectedLatitude, detectedLongitude) < 500.0;
//    }
//
//    private static double distance(double lat1, double lng1, double lat2, double lng2) {
//        Log.d(TAG, "distance() called with: lat1 = [" + lat1 + "], lng1 = [" + lng1 + "], lat2 = [" + lat2 + "], lng2 = [" + lng2 + "]");
//        Location unit = new Location("");
//        unit.setLatitude(lat1);
//        unit.setLongitude(lng1);
//        Location detected = new Location("");
//        detected.setLatitude(lat2);
//        detected.setLongitude(lng2);
//
//        Log.d("accurcy", String.valueOf((unit.getAccuracy())));
//        Log.d("accurcy", String.valueOf((unit.getAccuracy())));
//
//        Log.d(TAG, String.valueOf((unit.distanceTo(detected))));
//        return (unit.distanceTo(detected));
//    }
//}
    public static boolean locationIdintity(double unitLat, double unitLon, double detectedLatitude, double detectedLongitude) {

        return distance(unitLat, unitLon, detectedLatitude, detectedLongitude) < 500.0;
    }

    private static double distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75; // in miles, change to 6371 for kilometer output

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = earthRadius * c;

        Log.d("DistanceID", String.valueOf(dist));
        return dist; // output distance, in MILES
    }
}