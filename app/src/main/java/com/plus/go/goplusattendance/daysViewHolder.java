package com.plus.go.goplusattendance;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class daysViewHolder  extends RecyclerView.ViewHolder {

    public TextView mTitle, mDate, mDescription;

    public daysViewHolder(@NonNull View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.title_report);
        mDate=itemView.findViewById(R.id.time_report);
        mDescription=itemView.findViewById(R.id.day_description);

    }
}
