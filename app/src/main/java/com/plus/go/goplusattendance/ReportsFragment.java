package com.plus.go.goplusattendance;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.plus.go.goplusattendance.model.LoginOperation;
import com.plus.go.goplusattendance.utils.PrefrencesHandler;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportsFragment extends Fragment {

    NiceSpinner daysSpinner;
    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference mDatabaseReference;
    SharedPreferences pref;
    String userId, userCode, dayRef;
    private RecyclerView mDaystRecycler;

    List<String> daysDataSet;
    FirebaseRecyclerOptions<LoginOperation> options;
    FirebaseRecyclerAdapter<LoginOperation, daysViewHolder> adapter;

    public ReportsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reports, container, false);

        mDaystRecycler = view.findViewById(R.id.recyclerView_days);
        mDaystRecycler.setHasFixedSize(true);
        daysSpinner = view.findViewById(R.id.spinner_days);
        pref = getActivity().getSharedPreferences("User", MODE_PRIVATE);
        userCode = pref.getString("USER_CODE", "lll");
        userId = PrefrencesHandler.getInstance(getActivity()).getUserId();

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference("user").child(userId).child("Activities");
        daysDataSet = new ArrayList<>();
        Log.d("Ref", mDatabaseReference.toString());
        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Log.d("unitName", String.valueOf(snapshot.getKey()));
                    daysDataSet.add(String.valueOf(snapshot.getKey()));
                    daysSpinner.attachDataSource(daysDataSet);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        daysSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dayRef = String.valueOf(daysDataSet.get(daysSpinner.getSelectedIndex()));

                Toast.makeText(getActivity(), dayRef, Toast.LENGTH_LONG).show();

                mDatabaseReference.child(dayRef).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d("ReportData", dataSnapshot.getValue().toString());

                        options = new FirebaseRecyclerOptions.Builder<LoginOperation>()
                                .setQuery(mDatabaseReference.child(dayRef), LoginOperation.class).build();
                        adapter = new FirebaseRecyclerAdapter<LoginOperation, daysViewHolder>(options) {
                            @Override
                            protected void onBindViewHolder(@NonNull final daysViewHolder holder, final int position, @NonNull final LoginOperation model) {
                                holder.mTitle.setText(model.getOperationName());
                                holder.mDate.setText(model.getOperationTime());
                                holder.mDescription.setText(model.getOperationDescription());


                            }

                            @NonNull
                            @Override
                            public daysViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
                                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item, parent, false);

                                return new daysViewHolder(view);
                            }
                        };
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        mDaystRecycler.setLayoutManager(linearLayoutManager);
                        adapter.startListening();
                        mDaystRecycler.setAdapter(adapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null)
            adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null)
            adapter.stopListening();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null)
            adapter.startListening();
    }
}
