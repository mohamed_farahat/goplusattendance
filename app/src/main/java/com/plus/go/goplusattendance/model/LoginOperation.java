package com.plus.go.goplusattendance.model;

public class LoginOperation {

    String operationName,operationTime,operationUrl,operationDescription;

    public LoginOperation(String operationName, String operationTime, String operationUrl, String operationDescription) {
        this.operationName = operationName;
        this.operationTime = operationTime;
        this.operationUrl = operationUrl;
        this.operationDescription = operationDescription;
    }

    public LoginOperation() {
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public String getOperationUrl() {
        return operationUrl;
    }

    public void setOperationUrl(String operationUrl) {
        this.operationUrl = operationUrl;
    }

    public String getOperationDescription() {
        return operationDescription;
    }

    public void setOperationDescription(String operationDescription) {
        this.operationDescription = operationDescription;
    }
}
