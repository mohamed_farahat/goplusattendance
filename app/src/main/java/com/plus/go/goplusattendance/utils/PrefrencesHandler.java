package com.plus.go.goplusattendance.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Raed Saeed on 12/19/2018.
 */

public class PrefrencesHandler {
    private Context context;
    private static final String USER_CODE = "USER_CODE";
    private static final String USER_PASSWORD = "USER_PASSWORD";
    private static final String USER_GOV = "USER_GOV";
    private static final String USER_UNIT = "USER_UNIT";
    private static final String USER_TOKEN = "USER_TOKEN";
    private static final String USER_ID = "USER_ID";
    private static final String LOGIN_DATE = "LOGIN_DATE";
    private static final String LOGIN_TIME = "LOGIN_TIME";
    private static final String WORKING_STATUS = "WORKING_STATUٍS";
    private static final String LONGITUDE = "LONGITUDE_VALUE";
    private static final String LATITUDE = "LATITUDE_VALUE";
    private static final String DEV_REC_DATE = "DEV_REC_DATE";
    private static final String LAST_LOC_STATE = "LAST_LOC_STATE";
    private PrefrencesHandler(Context context) {
        this.context = context;
    }

    public static PrefrencesHandler getInstance(Context context) {
        return new PrefrencesHandler(context);
    }

    public void setUserCode(String userCode) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_CODE, userCode);
        editor.apply();
    }

    public String getUserCode() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_CODE, "0");
    }

    public void setUserPassword(String userPassword) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_PASSWORD, userPassword);
        editor.apply();
    }

    public String getUserPassword() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_PASSWORD, "0");
    }

    public void setUserGov(String userGov) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_GOV, userGov);
        editor.apply();
    }

    public String getUserGov() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_GOV, "0");
    }

    public void setUserUnit(String userUnit) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_UNIT, userUnit);
        editor.apply();
    }

    public String getUserUnit() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_UNIT, "0");
    }

    public void setUserToken (String userToken) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_TOKEN, userToken);
        editor.apply();
    }

    public String getUserToken () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_TOKEN, "0");
    }

    public void setUserId (String userId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID, userId);
        editor.apply();
    }

    public String getUserId () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_ID, "0");
    }

    public void setLoginDate (String loginDate) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGIN_DATE, loginDate);
        editor.apply();
    }

    public void setDevRecDate (String devRecDate) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(DEV_REC_DATE, devRecDate);
        editor.apply();
    }

    public String getDevRecDate () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(DEV_REC_DATE, "0");
    }

    public String getLoginDate () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(LOGIN_DATE, "0");
    }

    public void setLoginTime (String loginTime) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGIN_TIME, loginTime);
        editor.apply();
    }

    public String getLoginTime () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getString(LOGIN_TIME, "0");
    }

    public void setWorkingStatus (int workingStatus) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(WORKING_STATUS, workingStatus);
        editor.apply();
    }
    public int getWorkingStatus () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getInt(WORKING_STATUS, 0);
    }

    public void setUnitLongitude (double longitude) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(LONGITUDE, Double.doubleToRawLongBits(longitude));
        editor.apply();
    }

    public double getUnitLongitude () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return Double.longBitsToDouble(sharedPreferences.getLong(LONGITUDE, 0));
    }
    public void setUnitLatitude (double latitude) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(LATITUDE, Double.doubleToRawLongBits(latitude));
        editor.apply();
    }

    public double getUnitLatitude () {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return Double.longBitsToDouble(sharedPreferences.getLong(LATITUDE, 0));
    }

    public void setLastLocState(Boolean state) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LAST_LOC_STATE, state);
        editor.apply();
    }

    public Boolean getLastLocState() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(LAST_LOC_STATE, false);
    }
}
