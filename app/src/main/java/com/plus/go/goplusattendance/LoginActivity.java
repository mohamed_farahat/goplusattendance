package com.plus.go.goplusattendance;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.plus.go.goplusattendance.model.FirebaseUnit;
import com.plus.go.goplusattendance.model.FirebaseUser;
import com.plus.go.goplusattendance.model.LoginOperation;
import com.plus.go.goplusattendance.utils.DistanceCalculator;
import com.plus.go.goplusattendance.utils.NotificationHandler;
import com.plus.go.goplusattendance.utils.PrefrencesHandler;
import com.plus.go.goplusattendance.utils.Utils;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    FirebaseDatabase mDatabase;
    DatabaseReference mGovReference, mUserRef;
    NiceSpinner niceSpinnerGov, niceSpinnerUnits;
    List<String> govDataSet, unitsDataSet;
    String govRef;
    public static final int PICK_MAP_POINT_REQUEST = 999;
    public static final int GPS_REQUEST = 1;
    private FusedLocationProviderClient mFusedProviderClient;
    private LocationRequest mLocationRequest;
    public double detectedLatitude, detectedLongitude;
    LocationManager locationManager;
    double unitLat;
    double unitLon;
    NotificationHandler notificationHandler;

    EditText mUserCode, mUserPassword;
    ProgressBar mLoginProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        niceSpinnerGov = findViewById(R.id.gov_spinner);
        niceSpinnerUnits = findViewById(R.id.unit_spinner);
        mUserCode = findViewById(R.id.sigin_code_edittext);
        mUserPassword = findViewById(R.id.sigin_password_edittext);
        mLoginProgressBar = findViewById(R.id.login_progressBar);
        mDatabase = FirebaseDatabase.getInstance();

        notificationHandler = new NotificationHandler(this);
        notificationHandler.sendNotification("يوم جديد", "مرحبا بك");

        mGovReference = mDatabase.getReference("محافظات");
        mUserRef = mDatabase.getReference("user");
        govDataSet = new ArrayList<>();
        unitsDataSet = new ArrayList<>();
        requestLocationUpdates();

        mGovReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    govDataSet.add(snapshot.getKey());
                    niceSpinnerGov.attachDataSource(govDataSet);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mGovReference.child("الجيزة").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    unitsDataSet.add(snapshot.getKey());
                    niceSpinnerUnits.attachDataSource(unitsDataSet);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        niceSpinnerGov.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                govRef = String.valueOf(govDataSet.get(niceSpinnerGov.getSelectedIndex()));
                // Toast.makeText(LoginActivity.this, govRef, Toast.LENGTH_SHORT).show();
                unitsDataSet.clear();

                mGovReference.child(govRef).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            unitsDataSet.add(String.valueOf(snapshot.getKey()));
                            niceSpinnerUnits.attachDataSource(unitsDataSet);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        niceSpinnerUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                final String unitRef = unitsDataSet.get(niceSpinnerUnits.getSelectedIndex());
                govRef = govDataSet.get(niceSpinnerGov.getSelectedIndex());
                mGovReference.child(govRef).child(unitRef).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        DataSnapshot dataSnapshot1 = dataSnapshot.child(unitRef);
                        FirebaseUnit unit = (FirebaseUnit) dataSnapshot1.getValue();
                        if (unit != null) {
                            unitLat = unit.getLatitude();
                            unitLon = unit.getLongitude();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //notifyMe();
    }


    private void notifyMe() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String timeStamp = tsLong.toString();
        String date = Utils.getCurDate();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userReference = database.getReference().child("user").child(PrefrencesHandler.getInstance(this).getUserId())
                .child("Activities").child(date)
                .child("location");

        userReference.child(timeStamp).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.w(TAG, "onComplete: managed to write to database");
                }
            }
        });
    }


    private void checkUserWithinLocation(final String gov, final String munit, final String code, final String password) {
        // Get the stored unit location from the database in order to calculate the radius.
        mGovReference.child(gov).child(munit).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseUnit unit = dataSnapshot.getValue(FirebaseUnit.class);
                if (unit != null) {
                    unitLon = unit.getLongitude();
                    unitLat = unit.getLatitude();
                    if (locationIdintity(unitLat, unitLon, detectedLatitude, detectedLongitude)) {
                        mLoginProgressBar.setVisibility(View.VISIBLE);

                        checkUserCodeAndPassword(gov, munit, code, password);
                    } else {
                        Log.d("Idinticality", String.valueOf(unitLat) + " " +
                                String.valueOf(detectedLatitude) + " " +
                                String.valueOf(unitLon) + " " +
                                String.valueOf(detectedLongitude) + " ");
                        Toast.makeText(LoginActivity.this, "خارج منطقه العمل", Toast.LENGTH_LONG).show();
                    }


//                    Log.w(TAG, "onDataChange: detected lat" + detectedLatitude );
//                    Log.w(TAG, "onDataChange: detected lon" + detectedLongitude );
//                    Log.w(TAG, "onDataChange: lat" + unitLat );
//                    Log.w(TAG, "onDataChange: lon" + unitLon );
                } else {
                    Toast.makeText(LoginActivity.this, "DB location is not inserted yet", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(LoginActivity.this, "Location request Stopped", Toast.LENGTH_LONG).show();
            }

        });
    }

    private void checkUserCodeAndPassword(final String gov, final String unit, final String code, final String password) {
        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            boolean UserIsFound = false;

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, snapshot.getKey());
                    Log.d(TAG, mUserCode.getText().toString());
                    Log.d(TAG, snapshot.getKey());
                    FirebaseUser firebaseUser = snapshot.getValue(FirebaseUser.class);

                    if (code.equalsIgnoreCase(firebaseUser.getCode()) && password.equals(firebaseUser.getPassword())) {
                        if (firebaseUser.getGov().equals(gov) && firebaseUser.getUnit().equals(unit)) {
                            handleUserExist(snapshot.getKey(), gov, unit, code, password);
                        } else {
                            Toast.makeText(LoginActivity.this, "بيانات المكان غير صحيحه", Toast.LENGTH_LONG).show();
                        }
                        UserIsFound = true;
                        notificationHandler.sendNotification("نجاح عمليه الدخول", "مرحبا بك");

                        break;
                    }
                }

                if (!UserIsFound)
                    Toast.makeText(LoginActivity.this, "المستخدم غير موجود", Toast.LENGTH_LONG).show();
                mLoginProgressBar.setVisibility(View.GONE);
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void loginClicked(View view) {
        final String selectedGov, selectedUnit, code, password;
        selectedGov = govDataSet.get(niceSpinnerGov.getSelectedIndex());
        selectedUnit = unitsDataSet.get(niceSpinnerUnits.getSelectedIndex());
        code = mUserCode.getText().toString();
        password = mUserPassword.getText().toString();
        checkUserWithinLocation(selectedGov, selectedUnit, code, password);

    }

    private void handleUserExist(final String key, final String gov, final String unit, final String code, final String password) {
        Log.w(TAG, "handleUserExist: called");
        Log.d(TAG, "handleUserExist() called with: key = [" + key + "], gov = [" + gov + "]," +
                " unit = [" + unit + "], code = [" + code + "], password = [" + password + "]");
        PrefrencesHandler prefrencesHandler = PrefrencesHandler.getInstance(LoginActivity.this);
        prefrencesHandler.setUserCode(code);
        prefrencesHandler.setUserPassword(password);
        prefrencesHandler.setUserGov(gov);
        prefrencesHandler.setUserUnit(unit);
        prefrencesHandler.setUserId(key);
        prefrencesHandler.setUnitLatitude(unitLat);
        prefrencesHandler.setUnitLongitude(unitLon);

        String date = Utils.getCurDate();
        String loginTime = Utils.getCurTime();

        mLoginProgressBar.setVisibility(View.GONE);

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("loginDate", String.valueOf(date));
        Log.d("LoginTime", String.valueOf(date));
        Log.d("LoginTime", loginTime);

        prefrencesHandler.setLoginDate(date);
        prefrencesHandler.setLoginTime(loginTime);
        addLoginOperation(date, loginTime, key, "تسجيل دخول");
        startActivity(intent);
        finish();


    }

    private void requestLocationUpdates() {

        if (!isLocationEnabled(this)) {
            // Build the alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("تحديد الموقع غير مفعل");
            builder.setMessage("برجاء تفعيل تحديد الموقع");
            builder.setIcon(R.drawable.ic_location);
            builder.setPositiveButton("موافق", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Show location settings when the user acknowledges the alert dialog
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, GPS_REQUEST);
                }
            });
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PermissionChecker.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PermissionChecker.PERMISSION_GRANTED) {

            mFusedProviderClient = new FusedLocationProviderClient(this);
            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setFastestInterval(20000);
            mLocationRequest.setInterval(40000);


            mFusedProviderClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    if (locationResult.getLastLocation() != null) {
                        detectedLatitude = locationResult.getLastLocation().getLatitude();
                        detectedLongitude = locationResult.getLastLocation().getLongitude();

//                        Toast.makeText(LoginActivity.this,
//                                String.valueOf(detectedLatitude) + " : " + String.valueOf(detectedLongitude),
//                                Toast.LENGTH_SHORT).show();
                        Log.d("LatLong", String.valueOf(detectedLatitude) + " : " + String.valueOf(detectedLongitude));
                        if (!locationIdintity(unitLat, unitLon, detectedLatitude, detectedLongitude)) {
                            //    Toast.makeText(LoginActivity.this, "Location is more than 500", Toast.LENGTH_SHORT).show();

                        } else {
                            //   Toast.makeText(LoginActivity.this, "Locations is less than 500", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }, getMainLooper());
        } else {
            callPermissions();
        }
    }


    private boolean locationIdintity(double unitLat, double unitLon, double detectedLatitude, double detectedLongitude) {

        return distance(unitLat, unitLon, detectedLatitude, detectedLongitude) < 500.0;
    }


    private double distance(double lat1, double lng1, double lat2, double lng2) {
        float pk = (float) (180.f / Math.PI);
        Log.d(TAG, "distance() called with: lat1 = [" + lat1 + "], lng1 = [" + lng1 + "], lat2 = [" + lat2 + "], lng2 = [" + lng2 + "]");
        double a1 = lat1 / pk;
        double a2 = lng1 / pk;
        double b1 = lat2 / pk;
        double b2 = lng2 / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);
        double distance = 6366000 * tt;

        Log.d("DistanceID", String.valueOf(distance));

        return distance;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("GPSRequest : ", "request is :" + String.valueOf(requestCode) +
                "result is : " + resultCode);
        if (requestCode == GPS_REQUEST) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                    }, GPS_REQUEST);
                } else {
                    callPermissions();
                }
            }
        }
    }

    public void callPermissions() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Permissions.check(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, "Location Permission are required", new Permissions.Options()
                        .setSettingsDialogTitle("Warning").setRationaleDialogTitle("info"),
                new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        requestLocationUpdates();
                    }

                    @Override
                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                        super.onDenied(context, deniedPermissions);
                        callPermissions();
                    }
                });
    }


    /**
     * Add a new location for the user which will be used to calculate the distance each time the user sign in
     *
     * @param latitude
     * @param longitude
     */
    private void addLocationForUnit(double latitude, double longitude) {
        FirebaseUnit unit = new FirebaseUnit(latitude, longitude, "الدقي");
        mGovReference.child("الجيزة").child("الدقي").setValue(unit).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Successfully add a new unit", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    private void addLoginOperation(String date, String loginTime, String key, String operationName) {
        Long tsLong = System.currentTimeMillis() / 1000;
        String timeStamp = tsLong.toString();

        LoginOperation loginOperation = new LoginOperation(operationName, loginTime, "noImage", "بدء اليوم");
        mUserRef.child(key).child("Activities").child(date).child(timeStamp).setValue(loginOperation);
    }

    private void addNewUser() {
        Log.w(TAG, "addNewUser: called");
        FirebaseUser user = new FirebaseUser("32", "A002", "الجيزة", "Raed", "1234567", "012342345", "الدقي");
        DatabaseReference databaseReference = mUserRef.push();
        String key = databaseReference.getKey();
        PrefrencesHandler.getInstance(this).setUserId(key);
        Log.w(TAG, "addNewUser: " + key);
        databaseReference.setValue(user);
    }

}