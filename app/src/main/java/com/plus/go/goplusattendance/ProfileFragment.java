package com.plus.go.goplusattendance;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.plus.go.goplusattendance.model.FirebaseUser;
import com.plus.go.goplusattendance.utils.PrefrencesHandler;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    FirebaseDatabase mFirebaseDatabase;
    DatabaseReference mReference;
    SharedPreferences pref;
    String userCode, userName, userPhone, userAge, userUnit, userGov, userId;
    TextView userNameTextView, userPhoneTextView, userAgeTextView, userUnitTextView, userGovTextView;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        userNameTextView = view.findViewById(R.id.user_name);
        userPhoneTextView = view.findViewById(R.id.user_phone);
        userUnitTextView = view.findViewById(R.id.user_unit);
        userGovTextView = view.findViewById(R.id.userGov);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        pref = getActivity().getSharedPreferences("User", MODE_PRIVATE);
        userCode = pref.getString("USER_CODE", "lll");
        userId = PrefrencesHandler.getInstance(getActivity()).getUserId();

        mReference = mFirebaseDatabase.getReference("user").child(userId);
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                FirebaseUser user = dataSnapshot.getValue(FirebaseUser.class);
                userNameTextView.setText(user.getName());
                userPhoneTextView.setText(String.valueOf(user.getPhone()));
                userUnitTextView.setText(user.getUnit());
                userGovTextView.setText(user.getGov());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }

}
