package com.plus.go.goplusattendance.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

/**
 * Created by Raed Saeed on 12/19/2018.
 */

public class LocationDetector extends LocationCallback {
    private static final String TAG = "LocationDetector";
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Context context;


    public LocationDetector() {
        super();
    }

    private LocationDetector(Context context) {
        fusedLocationProviderClient = new FusedLocationProviderClient(context);
        mLocationRequest = new LocationRequest();
        this.context = context;
    }

    public static LocationDetector getInstance(Context context) {
        return new LocationDetector(context);
    }

    public void getLocation() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PermissionChecker.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PermissionChecker.PERMISSION_GRANTED) {
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setFastestInterval(20000);
            mLocationRequest.setInterval(40000);
            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, this, context.getMainLooper());
        }
    }

    @Override
    public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
    }

    @Override
    public void onLocationAvailability(LocationAvailability locationAvailability) {
        super.onLocationAvailability(locationAvailability);
    }
}
