package com.plus.go.goplusattendance.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.plus.go.goplusattendance.model.LoginOperation;
import com.plus.go.goplusattendance.utils.DistanceCalculator;
import com.plus.go.goplusattendance.utils.PrefrencesHandler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by shawara on 12/14/2016.
 */

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private static final int LOCATION_REQUEST_INTERVAL = 10 * 1000; //60 seconds
    private static final String TAG = "LocationService";
    private double lat = 0.0, lon = 0.0;
    private DatabaseReference mLocationRef;
    private PrefrencesHandler mPrefs;
    private FirebaseDatabase mDatabase;
    private boolean mLastLocationState = true;
    private FusedLocationProviderClient mFusedProviderClient;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mPrefs = PrefrencesHandler.getInstance(this);
        mDatabase = FirebaseDatabase.getInstance();

        mLocationRef = mDatabase.getReference("user/");
        ///mLocationRef.keepSynced(true);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
        mGoogleApiClient.connect();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        super.onDestroy();
    }

    private void initLocationRequest() {
        //  mLocationRequest= LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(LOCATION_REQUEST_INTERVAL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected: ");
        initLocationRequest();
    }



    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: ");
    }


    @Override
    public void onLocationChanged(Location location) {
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        String loginTime = String.valueOf(DateFormat.format("HH:mm", new Date()));
        Log.d(TAG, "onLocationChanged: " + mPrefs.getUnitLatitude() + "  " + mPrefs.getUnitLongitude() + "  " + location.getLatitude() + "  " + location.getLongitude());
        addLocationOperation(date, loginTime, mPrefs.getUserId(), "المكان", mLastLocationState);

        if (mPrefs.getLoginDate() == null || !mPrefs.getLoginDate().equals(date)) {
            mPrefs.setWorkingStatus(0);
            stopSelf();
        }

        //Log.d(TAG, "onLocationChanged: " + location.getLatitude() + " " + location.getLongitude());
        if (location.getLatitude() != lat || location.getLongitude() != lon) {
            lat = location.getLatitude();
            lon = location.getLongitude();
            boolean isDistanceOK = DistanceCalculator.locationIdintity(mPrefs.getUnitLatitude(), mPrefs.getUnitLongitude(), lat, lon);
            // mLocationRef.setValue(new LocationM(lat, lon, ServerValue.TIMESTAMP));
            if (isDistanceOK != mLastLocationState) {
                mLastLocationState = !mLastLocationState;
                addLocationOperation(date, loginTime, mPrefs.getUserId(), "المكان", mLastLocationState);


                if (!mLastLocationState) {
                    //Todo notification here that user is out of work area
                }
            }

        }
    }


    private void addLocationOperation(String date, String time, String key, String operationName, boolean isIn) {
        Long tsLong = System.currentTimeMillis() / 1000;
        String timeStamp = tsLong.toString();

        String Desc = (isIn ? "انت الان داخل منطقة العمل" : "انت الان خارج منطقة العمل");
        LoginOperation loginOperation = new LoginOperation(operationName, time, "noImage", "");
        mLocationRef.child(key).child("Activities").child(date).child(timeStamp).setValue(loginOperation);
    }



}