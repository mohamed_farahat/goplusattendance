package com.plus.go.goplusattendance;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plus.go.goplusattendance.model.LoginOperation;
import com.squareup.picasso.Picasso;

/**
 * Created by shawara on 12/25/2018.
 */

public class OperationHolder extends RecyclerView.ViewHolder {
    private ImageView mOperationImage;
    private TextView mTitleTextView;
    private TextView mDiscTextView;
    private TextView mTimeTextView;

    public OperationHolder(@NonNull View itemView, Context context) {
        super(itemView);
        mOperationImage = itemView.findViewById(R.id.activity_image);
        mTitleTextView = itemView.findViewById(R.id.activity_title);
        mDiscTextView = itemView.findViewById(R.id.activity_desc);
        mTimeTextView = itemView.findViewById(R.id.activity_time);
    }

    public void bindData(LoginOperation operation) {
        mTitleTextView.setText(operation.getOperationName());
        mDiscTextView.setText(operation.getOperationDescription());
        mTimeTextView.setText(operation.getOperationTime());
        if (!operation.getOperationUrl().equals("noImage")) {
            mOperationImage.setVisibility(View.VISIBLE);
            Picasso.get().load(operation.getOperationUrl()).into(mOperationImage);
        }else{
            mOperationImage.setVisibility(View.GONE);
        }
    }
}
