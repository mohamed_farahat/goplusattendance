package com.plus.go.goplusattendance;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.google.firebase.database.DatabaseReference;
import com.plus.go.goplusattendance.utils.PrefrencesHandler;
import com.plus.go.goplusattendance.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SplashActivtiy extends AppCompatActivity {
    DatabaseReference mDatabase;
    private static final int SPLASH_TIME_OUT = 2000;
    private PrefrencesHandler mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = PrefrencesHandler.getInstance(this);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_activtiy);


    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
                anim.setInterpolator(new LinearInterpolator());
                anim.setRepeatCount(Animation.INFINITE);
                anim.setDuration(700);

                final ImageView splash = findViewById(R.id.logo);
                splash.startAnimation(anim);

                splash.setAnimation(null);

                String date = Utils.getCurDate();
                //allow user to recive device if it is a new day

                if (!mPrefs.getDevRecDate().equals(date))
                    mPrefs.setWorkingStatus(0);

                if (mPrefs.getLoginDate() == null || !mPrefs.getLoginDate().equals(date)) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("loginDate", String.valueOf(date));
                    startActivity(intent);
                }
            }
        }, SPLASH_TIME_OUT);
    }
}