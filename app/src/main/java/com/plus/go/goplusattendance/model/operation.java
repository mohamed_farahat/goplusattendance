package com.plus.go.goplusattendance.model;

public class operation {

    String title, date,desc;

    public operation(String title, String date, String desc) {
        this.title = title;
        this.date = date;
        this.desc = desc;
    }

    public operation() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
