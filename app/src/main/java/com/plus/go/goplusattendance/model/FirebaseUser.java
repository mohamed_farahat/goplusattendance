package com.plus.go.goplusattendance.model;

public class FirebaseUser {

    String age, code, gov, name, password, phone, unit;
    private String token;
    private String userId;

    public FirebaseUser(String age, String code, String gov, String name, String password, String phone, String unit) {
        this.age = age;
        this.code = code;
        this.gov = gov;
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.unit = unit;
    }

    public FirebaseUser() {
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGov() {
        return gov;
    }

    public void setGov(String gov) {
        this.gov = gov;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getUserId () {
        return userId;
    }

    public void setUserId (String userId) {
        this.userId = userId;
    }
    @Override
    public String toString() {
        return "FirebaseUser{" +
                "age='" + age + '\'' +
                ", code='" + code + '\'' +
                ", gov='" + gov + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}
