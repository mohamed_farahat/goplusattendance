package com.plus.go.goplusattendance;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.plus.go.goplusattendance.model.LoginOperation;
import com.plus.go.goplusattendance.utils.DistanceCalculator;
import com.plus.go.goplusattendance.utils.PrefrencesHandler;
import com.plus.go.goplusattendance.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static android.os.Looper.getMainLooper;
import static com.plus.go.goplusattendance.LoginActivity.GPS_REQUEST;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    static final int REQUEST_IMAGE_CAPTURE = 1, SELECT_FILE = 0;

    Dialog myDialog;
    ImageView mRecive, imageCamer;
    String loginDate, loginTime, userCode;
    TextView mLoginTime;
    EditText mHeldDesc;
    FirebaseStorage firebaseStorage;
    StorageReference storageReference;
    SharedPreferences pref;
    LinearLayout startLinear;
    private String userId;
    int workingStatus;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference userReference;
    private Context context;
    private LocationManager locationManager;
    private FusedLocationProviderClient mFusedProviderClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private double detectedLatitude, detectedLongitude;
    private double unitLat, unitLon;
    private PrefrencesHandler mPrefs;
    private RecyclerView mRecyclerView;
    private Query mActivityQuires;
    private FirebaseRecyclerAdapter mAdapter;
    private boolean mLastLocationState = true;
    private double lat = 0.0, lon = 0.0;
    private DatabaseReference mLocationRef;
    private FirebaseDatabase mDatabase;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        mPrefs = PrefrencesHandler.getInstance(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mRecive = view.findViewById(R.id.recive_item);
        mRecyclerView = view.findViewById(R.id.activities_recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mDatabase = FirebaseDatabase.getInstance();
        mLocationRef = mDatabase.getReference("user/");
        mActivityQuires = FirebaseDatabase.getInstance()
                .getReference()
                .child("user").child(mPrefs.getUserId()).child("Activities").child(Utils.getCurDate());


        FirebaseRecyclerOptions<LoginOperation> options =
                new FirebaseRecyclerOptions.Builder<LoginOperation>()
                        .setQuery(mActivityQuires, LoginOperation.class)
                        .build();


        mAdapter = new FirebaseRecyclerAdapter<LoginOperation, OperationHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull OperationHolder holder, int position, @NonNull LoginOperation model) {
                holder.bindData(model);
            }

            @Override
            public OperationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_activity, parent, false);

                return new OperationHolder(view, getContext());
            }

        };

        mRecyclerView.setAdapter(mAdapter);

        checkRecived();
        myDialog = new Dialog(getActivity());

        mRecive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txtclose;
                Button btnFollow;
                myDialog.setContentView(R.layout.custompopup);

                btnFollow = (Button) myDialog.findViewById(R.id.btnfollow);
                mHeldDesc = myDialog.findViewById(R.id.held_Desc);
                imageCamer = myDialog.findViewById(R.id.openCamera);
                imageCamer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    REQUEST_IMAGE_CAPTURE);
                        }

                    }
                });
                btnFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleUploadImage();
                        // handleReciveHeldOperation("استلام الجهاز", mHeldDesc.getText().toString());
                        myDialog.dismiss();
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();
            }
        });

        unitLat = PrefrencesHandler.getInstance(context).getUnitLatitude();
        unitLon = PrefrencesHandler.getInstance(context).getUnitLongitude();
        Log.d("SharedLatLon", String.valueOf(unitLat) + " : " + String.valueOf(unitLon));
        userId = PrefrencesHandler.getInstance(context).getUserId();
        firebaseDatabase = FirebaseDatabase.getInstance();


        requestLocationUpdates();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    private void requestLocationUpdates() {
        Log.w(TAG, "requestLocationUpdates: called");
        if (!isLocationEnabled(context)) {
            // Build the alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("تحديد الموقع غير مفعل");
            builder.setMessage("برجاء تفعيل تحديد الموقع");
            builder.setIcon(R.drawable.ic_location);
            builder.setPositiveButton("موافق", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    // Show location settings when the user acknowledges the alert dialog
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, GPS_REQUEST);
                }
            });
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        }

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PermissionChecker.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PermissionChecker.PERMISSION_GRANTED) {

            mFusedProviderClient = new FusedLocationProviderClient(context);
            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setFastestInterval(20000);
            mLocationRequest.setInterval(40000);


            mFusedProviderClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    lat = locationResult.getLastLocation().getLatitude();
                    lon = locationResult.getLastLocation().getLongitude();
                    Log.d("Accurcy", String.valueOf(locationResult.getLastLocation().getAccuracy()));
                    boolean isDistanceOK = DistanceCalculator.locationIdintity(mPrefs.getUnitLatitude(), mPrefs.getUnitLongitude(), lat, lon);
                    // mLocationRef.setValue(new LocationM(lat, lon, ServerValue.TIMESTAMP));
                    mLastLocationState = PrefrencesHandler.getInstance(context).getLastLocState();
                    if (isDistanceOK != mLastLocationState) {
                        mLastLocationState = !mLastLocationState;
                        addLocationOperation(date, loginTime, mPrefs.getUserId(), "المكان", mLastLocationState);

                        if (mLastLocationState) {
                            sendNotification("داخل منطقه العمل", "عوده");
                        } else {
                            sendNotification("خارج منطقه العمل", "تحذير");
                        }
                        PrefrencesHandler.getInstance(context).setLastLocState(mLastLocationState);
                    }
                }
            }, getMainLooper());
        } else {
            callPermissions();
        }
    }

    private void addLocationOperation(String date, String time, String key, String operationName, boolean isIn) {
        Long tsLong = System.currentTimeMillis() / 1000;
        String timeStamp = tsLong.toString();

        String Desc = (isIn ? "انت الان داخل منطقة العمل" : "انت الان خارج منطقة العمل");
        LoginOperation loginOperation = new LoginOperation(operationName, time, "noImage", "");
        mLocationRef.child(key).child("Activities").child(date).child(timeStamp).setValue(loginOperation);
    }

    private boolean locationIdintity(double unitLat, double unitLon, double detectedLatitude, double detectedLongitude) {

        return distance(unitLat, unitLon, detectedLatitude, detectedLongitude) < 500.0;
    }


    private double distance(double lat1, double lng1, double lat2, double lng2) {
        float pk = (float) (180.f / Math.PI);
        Log.d(TAG, "distance() called with: lat1 = [" + lat1 + "], lng1 = [" + lng1 + "], lat2 = [" + lat2 + "], lng2 = [" + lng2 + "]");
        double a1 = lat1 / pk;
        double a2 = lng1 / pk;
        double b1 = lat2 / pk;
        double b2 = lng2 / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);
        double distance = 6366000 * tt;

        Log.d("DistanceID", String.valueOf(distance));

        return distance;
    }

    public void sendNotification(String bigTitle, String smallTitle) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, "notify_001notify_001");
        Intent ii = new Intent(context, LoginActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.setBigContentTitle(bigTitle);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher_round);
        mBuilder.setContentText(smallTitle);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "YOUR_CHANNEL_ID";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());

    }

    public void callPermissions() {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        Permissions.check(context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, "Location Permission are required", new Permissions.Options()
                        .setSettingsDialogTitle("Warning").setRationaleDialogTitle("info"),
                new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        requestLocationUpdates();
                    }

                    @Override
                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                        super.onDenied(context, deniedPermissions);
                        callPermissions();
                    }
                });
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    private void checkRecived() {
        if (workingStatus == 1) {
            mRecive.setImageResource(R.drawable.ic_hand_held_green);
            mRecive.setEnabled(false);

        } else {
            mRecive.setImageResource(R.drawable.ic_hand_held);
            mRecive.setEnabled(true);

        }
    }

    private void notifyMe() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String timeStamp = tsLong.toString();

        firebaseDatabase = FirebaseDatabase.getInstance();
        userReference = firebaseDatabase.getReference().child("user").child(userId)
                .child("Activities").child(loginDate)
                .child("location");

        userReference.child(timeStamp).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.w(TAG, "onComplete: managed to write to database");
                }
            }
        });
    }

    private void handleUploadImage() {
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        final StorageReference storageReference = firebaseStorage.getReference()
                .child("images" + UUID.randomUUID().toString());


        imageCamer.setDrawingCacheEnabled(true);
        imageCamer.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) imageCamer.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        final UploadTask uploadTask = storageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                // ...
                if (uploadTask.isSuccessful()) {
                    Log.d("UploadTask", String.valueOf(taskSnapshot.getUploadSessionUri()));
                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            handleReciveHeldOperation(String.valueOf(uri));

                        }
                    });
                }
            }
        });

    }

    private void handleReciveHeldOperation(String downloadUrl) {
        Log.d(TAG, "handleReciveHeldOperation: " + downloadUrl);

        Long tsLong = System.currentTimeMillis() / 1000;
        String timeStamp = tsLong.toString();

        LoginOperation reciveHeldOperation = new
                LoginOperation("استلام الجهاز", Utils.getCurTime(), String.valueOf(downloadUrl), mHeldDesc.getText().toString());
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
//        Log.d("userCode", userCode);
        databaseReference.child("user").child(mPrefs.getUserId()).child("Activities").child(Utils.getCurDate()).child(timeStamp).setValue(reciveHeldOperation);
        mPrefs.setWorkingStatus(1);
        mPrefs.setDevRecDate(Utils.getCurDate());
    }


    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_IMAGE_CAPTURE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

            } else {

                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();

            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                imageCamer.setImageBitmap(imageBitmap);
                mRecive.setImageResource(R.drawable.ic_hand_held_green);
                mRecive.setEnabled(false);
            }
        } else if (requestCode == GPS_REQUEST) {
            Log.e("GPSRequest : ", "request is :" + String.valueOf(requestCode) +
                    "result is : " + resultCode);

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                    }, GPS_REQUEST);
                } else {
                    callPermissions();
                }
            }
        }

    }
}
